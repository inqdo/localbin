E-wise local bin
================
*Do not manually add scripts or programs to this directory!*

## E-wise custom scripts

All E-wise custom scripts should be added to the ```localbin``` repository.
Ansible is used to checkout the repository in ```/var/local/repo/localbin```. All files in the repository are symlinked in ```/usr/local/bin```

## Ansible managed

Some programs can not be installed trough the packagemanager. Ansible will place these here but should not be managed in the ```localbin``` repository.
